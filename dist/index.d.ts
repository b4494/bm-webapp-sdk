export interface HandlerOptions {
    sessionHash?: string;
    scenarioCode?: string;
    initVariables?: TData;
    botTokenParamName?: string;
    userApiToken?: string;
    retryDelayOnTimeout?: number;
    requestTimeout?: number;
    responseVar?: string | ((handler: WebAppHandler) => any);
    apiEndpoint?: string;
}
export type TData = {
    [key: string]: any;
};
export interface INodeEventsDecl {
    inbox: boolean;
    outbox: boolean;
    anyInbox: boolean;
    dialogClosed: boolean;
    invoicePaid: boolean;
    externalRequest: boolean;
    buttons: boolean;
    customEvents: boolean;
}
export interface INode {
    id: number;
    title: string;
    typeName: "basic" | "event" | "eventListener" | "condition" | "conditionBranch";
    waitingForEvents?: INodeEventsDecl;
}
export type TScenarioSessionStatus = "active" | "stopped" | "ended" | "unknownError" | "tunnelDisabled" | "expired" | "userDisabled" | "stepDeleted" | "transitionsLimit" | "parentStopped";
export interface ScenarioSession {
    id: number;
    hash: string;
    statusName: TScenarioSessionStatus;
    ref: string | null;
    variables: TData;
    isTransitioning: boolean;
    currentNode: INode | null;
}
export interface ICurrentRequest {
    controller: AbortController | null;
    timeoutId: number;
    retryTimeoutId?: number;
    reject?: (reason?: any) => void;
}
export default class WebAppHandler {
    readonly options: HandlerOptions;
    data?: ScenarioSession;
    private currentRequest?;
    constructor(options: HandlerOptions);
    isActive(): boolean;
    getVariable(name: string): any;
    init(): Promise<WebAppHandler>;
    refreshData(): Promise<WebAppHandler>;
    protected requestResponse(): WebAppHandler | any;
    request(code: string, params?: TData, async?: boolean): Promise<WebAppHandler | any>;
    abort(): void;
    isTelegramInitAvailable(): boolean;
    protected initNewSession(): Promise<WebAppHandler>;
    protected makeRequest(method: string, path: string, json?: TData, signal?: AbortSignal): Promise<any>;
    private waitUntilSessionFinishesTransition;
}

export { WebAppHandler };
declare global {
    interface Window {
        Telegram: any;
    }
}

