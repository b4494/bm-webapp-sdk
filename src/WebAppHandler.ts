export interface HandlerOptions {
  sessionHash?: string,
  scenarioCode?: string,
  initVariables?: TData,
  botTokenParamName?: string,
  userApiToken?: string,
  retryDelayOnTimeout?: number,
  requestTimeout?: number,
  responseVar?: string|((handler: WebAppHandler) => any),
  apiEndpoint?: string,
}

export type TData = {
  [key: string]: any,
}

export interface INodeEventsDecl {
  inbox: boolean,
  outbox: boolean,
  anyInbox: boolean,
  dialogClosed: boolean,
  invoicePaid: boolean,
  externalRequest: boolean,
  buttons: boolean,
  customEvents: boolean,
}

export interface INode {
  id: number,
  title: string,
  typeName: "basic" | "event" | "eventListener" | "condition" | "conditionBranch",
  waitingForEvents?: INodeEventsDecl,
}

export type TScenarioSessionStatus = "active" | "stopped" | "ended" | "unknownError" | "tunnelDisabled"
    | "expired" | "userDisabled" | "stepDeleted" | "transitionsLimit" | "parentStopped";

export interface ScenarioSession {
  id: number,
  hash: string,
  statusName: TScenarioSessionStatus,
  ref: string|null,
  variables: TData,
  isTransitioning: boolean,
  currentNode: INode | null,
}

export interface ICurrentRequest {
  controller: AbortController|null;
  timeoutId: number;
  retryTimeoutId?: number;
  reject?: (reason?: any) => void,
}

const DEFAULTS: HandlerOptions = {
  retryDelayOnTimeout: 2000,
  requestTimeout: 60000,
  apiEndpoint: 'https://console.bot-marketing.com/api/public',
};

export default class WebAppHandler {
  readonly options: HandlerOptions;
  data?: ScenarioSession;
  private currentRequest?: ICurrentRequest;

  constructor(options: HandlerOptions) {
    this.options = { ...DEFAULTS, ...options };
  }

  isActive(): boolean {
    return this.data?.statusName === "active";
  }

  getVariable(name: string): any {
    return (this.data?.variables||{})[name];
  }

  async init(): Promise<WebAppHandler> {
    if (this.data) throw new Error("Handler already initialized");

    if (this.options.sessionHash) {
      await this.refreshData();

      if (!this.isActive() && this.isTelegramInitAvailable()) return this.initNewSession();

      return this;
    } else if (this.isTelegramInitAvailable()) {
      return this.initNewSession();
    } else {
      throw new Error('Failed to init handler. You must specify `sessionHash`, or `scenarioCode` when working with Telegram');
    }
  }

  async refreshData(): Promise<WebAppHandler> {
    const sessionId = this.data?.hash || this.options.sessionHash;

    if (!sessionId) throw new Error('Cannot refresh data: session id is missing');

    try {
      this.data = await this.makeRequest("GET", `tunnelSessions/${sessionId}`);

      return this;
    }

    catch (error) {
      throw new Error(`Failed to get session data: ${error.message}`);
    }
  }

  protected requestResponse(): WebAppHandler|any {
    if (!this.options.responseVar) return this;

    if (typeof this.options.responseVar === 'function') {
      return this.options.responseVar(this);
    }

    return (this.data.variables||{})[this.options.responseVar];
  }

  async request(code: string, params?: TData, async?: boolean): Promise<WebAppHandler|any> {
    if (this.currentRequest) throw new Error('Please wait until pending request is finished');

    if (!this.data) await this.init();

    if (!this.isActive()) throw new Error('Cannot make request: session is not active');

    if (async) {
      // noinspection ES6MissingAwait
      this.makeRequest("POST", `tunnelSessions/${this.data.hash}/request`, { code, params });

      return this;
    }

    try {
      this.currentRequest = {
        controller: new AbortController(),
        timeoutId: setTimeout(() => this.abort(), this.options.requestTimeout),
      };

      this.data = await this.makeRequest(
          "POST",
          `tunnelSessions/${this.data.hash}/request`,
          { code, params, },
          this.currentRequest.controller.signal,
      );

      this.currentRequest = null;

      return this.requestResponse();
    }

    catch (error) {
      if (error.message === 'request_timeout') return this.waitUntilSessionFinishesTransition();

      this.currentRequest = null;

      throw error;
    }
  }

  abort () {
    if (!this.currentRequest) return;

    // Main request is still running, we can just abort it
    // The rest is done inside request method
    if (this.currentRequest.controller) {
      this.currentRequest.controller.abort('Request timeout');
    } else {
      // At this point we are trying to wait until session is finished transitioning
      clearTimeout(this.currentRequest.retryTimeoutId);
      this.currentRequest.reject('Request timeout');
      this.currentRequest = null;
    }
  }

  isTelegramInitAvailable(): boolean {
    return window.Telegram !== undefined && this.options.scenarioCode !== undefined;
  }

  protected async initNewSession (): Promise<WebAppHandler> {
    try {
      this.data = await this.makeRequest("POST", `tunnel/${this.options.scenarioCode}/initWebApp`, {
        initData: Telegram.WebApp.initData,
        botTokenParamName: this.options.botTokenParamName,
        variables: this.options.initVariables,
      });

      return this;
    }

    catch (error) {
      throw new Error(`Failed to init new session: ${error.message}`);
    }
  }

  protected async makeRequest(method: string, path: string, json?: TData, signal?: AbortSignal): Promise<any> {
    const resp = await fetch(`${this.options.apiEndpoint}/${path}`, {
      method: method,
      signal,
      ...(method === "POST" ? {
        body: JSON.stringify(json),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
      } : {
        headers: { "Accept": "application/json" },
      }),
    });

    if (resp.status >= 400) {
      const data = await resp.json();

      throw new Error(data.message || `Http Error ${resp.status}`);
    }

    return resp.json();
  }

  private waitUntilSessionFinishesTransition(): Promise<WebAppHandler> {
    return new Promise((resolve, reject): void => {
      this.currentRequest.reject = reject;

      const resetTimeout = (): void => {
        this.currentRequest.retryTimeoutId = setTimeout(async (): Promise<void> => {
          await this.refreshData();

          if (this.data.isTransitioning) {
            resetTimeout();
          } else {
            try {
              resolve(this.requestResponse());
            }

            catch (e) {
              reject(e);
            }
          }
        }, this.options.retryDelayOnTimeout);
      };

      resetTimeout();
    });
  }
}