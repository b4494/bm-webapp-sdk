"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DEFAULTS = {
    retryDelayOnTimeout: 2000,
    requestTimeout: 60000,
    responseVar: 'response',
    apiEndpoint: 'https://console.bot-marketing.com/api/public',
};
class WebAppHandler {
    constructor(options) {
        this.options = { ...DEFAULTS, ...options };
    }
    isActive() {
        return this.data?.statusName === "active";
    }
    async init() {
        if (this.data)
            throw new Error("Handler already initialized");
        if (this.options.sessionHash) {
            await this.refreshData();
            if (!this.isActive() && this.isTelegramInitAvailable())
                return this.initNewSession();
            return this;
        }
        else if (this.isTelegramInitAvailable()) {
            return this.initNewSession();
        }
        else {
            throw new Error('Failed to init handler. You must specify `sessionId`, or `scenarioId` when working with Telegram');
        }
    }
    async refreshData() {
        const sessionId = this.data?.hash || this.options.sessionHash;
        if (!sessionId)
            throw new Error('Cannot refresh data: session id is missing');
        try {
            this.data = await this.makeRequest("GET", `tunnelSessions/${sessionId}`);
            return this;
        }
        catch (error) {
            throw new Error(`Failed to get session data: ${error.message}`);
        }
    }
    async request(code, params, async) {
        if (this.currentRequest)
            throw new Error('Please wait until pending request is finished');
        if (!this.data)
            await this.init();
        if (!this.isActive())
            throw new Error('Cannot make request: session is not active');
        if (async) {
            // noinspection ES6MissingAwait
            this.makeRequest("POST", `tunnelSessions/${this.data.hash}/request`, { code, params });
            return this;
        }
        try {
            this.currentRequest = {
                controller: new AbortController(),
                timeoutId: setTimeout(() => this.abort(), this.options.requestTimeout),
            };
            this.data = await this.makeRequest("POST", `tunnelSessions/${this.data.hash}/request`, { code, params, }, this.currentRequest.controller.signal);
            this.currentRequest = null;
            return this;
        }
        catch (error) {
            if (error.message === 'request_timeout')
                return this.waitUntilSessionFinishesTransition();
            this.currentRequest = null;
            throw error;
        }
    }
    abort() {
        if (!this.currentRequest)
            return;
        // Main request is still running, we can just abort it
        // The rest is done inside request method
        if (this.currentRequest.controller) {
            this.currentRequest.controller.abort('Request timeout');
        }
        else {
            // At this point we are trying to wait until session is finished transitioning
            clearTimeout(this.currentRequest.retryTimeoutId);
            this.currentRequest.reject('Request timeout');
            this.currentRequest = null;
        }
    }
    isTelegramInitAvailable() {
        return window.Telegram !== undefined && this.options.scenarioCode !== undefined;
    }
    async initNewSession() {
        try {
            this.data = await this.makeRequest("POST", `tunnel/${this.options.scenarioCode}/initWebApp`, {
                initData: Telegram.WebApp.initData,
                botTokenParamName: this.options.botTokenParamName,
                variables: this.options.initVariables,
            });
            return this;
        }
        catch (error) {
            throw new Error(`Failed to init new session: ${error.message}`);
        }
    }
    async makeRequest(method, path, json, signal) {
        const resp = await fetch(`${this.options.apiEndpoint}/${path}`, {
            method: method,
            signal,
            ...(method === "POST" ? {
                body: JSON.stringify(json),
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                },
            } : {
                headers: { "Accept": "application/json" },
            }),
        });
        if (resp.status >= 400) {
            const data = await resp.json();
            throw new Error(data.message || `Http Error ${resp.status}`);
        }
        return resp.json();
    }
    waitUntilSessionFinishesTransition() {
        return new Promise((resolve, reject) => {
            this.currentRequest.reject = reject;
            const resetTimeout = () => {
                this.currentRequest.retryTimeoutId = setTimeout(async () => {
                    await this.refreshData();
                    if (this.data.isTransitioning) {
                        resetTimeout();
                    }
                    else {
                        resolve(this);
                    }
                }, this.options.retryDelayOnTimeout);
            };
            resetTimeout();
        });
    }
}
exports.default = WebAppHandler;
