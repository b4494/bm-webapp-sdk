# Installation

## CDN

```html
<script src="https://cdn.jsdelivr.net/npm/bm-webapp-sdk@latest/dist/bm-webapp-sdk.min.js"></script>
```

## NPM

```
npm i --save-dev bm-webapp-sdk
```

# Usage

## Browser

Include script from CDN. You can then use a script like that:

```javascript
const handler = new BotMarketing.WebAppHandler({
  scenarioCode: 'my-scenario-code',
});

await handler.init();

if (handler.isActive()) {
  await handler.request('my-request-code', { param: 'value' });
  
  // To get any value from session variables
  handler.data.variables.response;
}
```

## Webpack

Import handler class:

```javascript
import { WebAppHandler } from 'bm-webapp-sdk/es2019';
```

You can then basically do same as for browser:

```javascript
const handler = new WebAppHandler({
  scenarioCode: 'my-scenario-code',
  responseVar: 'result',
});

await handler.init();

if (handler.isActive()) {
  // `resp` will hold value from `data.variables['result']` as `result` is set as `responseVar`
  const resp = await handler.request('my-request-code', { param: 'value' });
  
  // ...
}
```

## Telegram support

When running inside Telegram, handler can automatically init session. You need to pass `scenarioCode` when initializing
the handler.

Scenario must be within a project, project must have telegram api token defined in param `tg_api_token`. You can change
param name by setting `botTokenParamName` option. Also, project can have only one telegram bot messenger set in project settings.

__Remember that you have still to include telegram mini app scripts to enable telegram support.__

## Request timeout

This library automatically handles request timeout which is 20s by default. If scenario is handling request too long,
we will wait until it finishes handling by polling session data each `retryDelayOnTimeout` (2000 by default) milliseconds.
But at most `requestTimeout` (60000) milliseconds.

## Options

| Option                | Default                                        | Description                                                                                                                                                                                                                                                                 |
|-----------------------|------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `sessionHash`         | unset                                          | If specified, handler will use this session                                                                                                                                                                                                                                 |
| `scenarioCode`        | unset                                          | Scenario code that is used to initialize session. Applicable in telegram mini app only.                                                                                                                                                                                     |
| `initVariables`       | unset                                          | Variables that will be set on initializatation                                                                                                                                                                                                                              |
| `botTokenParamName`   | `tg_api_token`                                 | The name of the param of the project that holds telegram api token                                                                                                                                                                                                          |
| `userApiToken`        | unset                                          | Api token to authorize requests                                                                                                                                                                                                                                             |
| `retryDelayOnTimeout` | 3000                                           | On request timeout, we will retry each N milliseconds until session finishes handling request                                                                                                                                                                               |
| `requestTimeout`      | 60000                                          | Total request timeout                                                                                                                                                                                                                                                       |
| `responseVar`         | unset                                          | The name of the variable that holds response to request. Calling `handler.request()` will return the value of that variable when request finishes. It can also be a function that accepts handler instance and must return something that will be used as request response. |
| `apiEndpoint`         | `https://console.bot-marketing.com/api/public` | Api endpoint                                                                                                                                                                                                                                                                |